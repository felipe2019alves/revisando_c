﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Revisando
{
    /// <summary>
    /// Lógica interna para FrmCadSusp.xaml
    /// </summary>
    public partial class FrmCadSusp : Window
    {
        public FrmCadSusp()
        {
            InitializeComponent();
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtIdade.IsEnabled = true;
                txtIdade.Focus();
                btnNovo.IsEnabled = false;
                btnDell.IsEnabled = true;
            }
        }

        private void txtIdade_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtEnd.IsEnabled = true;
                txtEnd.Focus();
            }
        }

        private void txtEnd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtN.IsEnabled = true;
                txtN.Focus();
            }
        }

        private void txtN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtCep.IsEnabled = true;
                txtCep.Focus();
            }
        }

        private void txtCep_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtDate.IsEnabled = true;
                txtDate.Focus();
            }
        }

        private void txtDate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtStatus.IsEnabled = true;
                txtStatus.Focus();
                btnSalvar.IsEnabled = true;
                btnSalvar.Focus();
            }
        }

        private void btnDell_Click(object sender, RoutedEventArgs e)
        {
            txtNome.Clear();
            txtIdade.Clear();
            txtEnd.Clear();
            txtN.Clear();
            txtDate.Clear();
            txtCep.Clear();
            txtStatus.Clear();
            btnNovo.IsEnabled = true;
            btnNovo.Focus();
        }
    }
}

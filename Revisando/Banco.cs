﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Revisando
{
    public class Banco
    {
        string caminho = "SERVER=localhost;USER=root;DATABASE=biotec";

        public MySqlConnection conexao;

        public void conectar()
        {
            try
            {
                conexao = new MySqlConnection(caminho);
                conexao.Open();
            }
            catch 
            {
                System.Windows.MessageBox.Show("Erro Ao estabalecer conexão com o banco de dados", "ERRO");
            }


        }
        public void Desconectar() 
        {
            try
            {
                conexao = new MySqlConnection(caminho);
                conexao.Close();
            }
            catch
            {
                System.Windows.MessageBox.Show("Erro ao tentar se comunicar com o Banco de Dados", "ERRO");
            }
        }




    }
}

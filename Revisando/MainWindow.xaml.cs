﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Revisando
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        int i = 0;
        string usuario, senha;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnSair_Click(object sender, RoutedEventArgs e)
        {
            var sair = MessageBox.Show(
                "Tem certeza que deseja ENCERRAR o sistema?", "ENCERRAR SISTEMA",
                MessageBoxButton.YesNo,
                MessageBoxImage.Exclamation);
            if(sair != MessageBoxResult.Yes)
            {
                return;
            }
            else
            {
                Application.Current.Shutdown();
            }
        
        
        
        
        }

        private void txtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                usuario = txtEmail.Text;
                Pass.IsEnabled = true;
                Pass.Focus();

            }
        }

        private void Pass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                senha = Pass.Password;
                btnEntrar.IsEnabled = true;
                btnEntrar.Focus();
            }
        }

        private void btnEntrar_Click(object sender, RoutedEventArgs e)
        {

            Banco bd = new Banco();
            bd.conectar();
            MySqlCommand comm = new MySqlCommand("SELECT * FROM agente WHERE userAgente = @usuario AND senhaAgente = @senha", bd.conexao);
            comm.Parameters.Clear();
            comm.Parameters.Add("@usuario", MySqlDbType.String).Value = txtEmail.Text;
            comm.Parameters.Add("@senha", MySqlDbType.String).Value = Pass.Password;

            try
            {
                comm.CommandType = CommandType.Text;

                MySqlDataReader dr = comm.ExecuteReader();
                dr.Read();

                usuario = dr.GetString(5);

                btnEntrar.IsEnabled = false;
                System.Threading.Thread.Sleep(10);
                PbgLogin.Value = 0;
                Task.Run(() =>
                {
                    while (i < 100)
                    {
                        i++;
                        System.Threading.Thread.Sleep(50);
                        this.Dispatcher.Invoke(() => //usar para atualização imediata(obrigatoria)
                        {
                            PbgLogin.Value = i;
                            txbCarrega.Text = i + "%";
                            while (i == 100)
                            {

                                if (usuario == "Soldado")
                                {
                                    Hide();
                                    FrmMenu frm = new FrmMenu();
                                    frm.Show();
                                    break;
                                }

                                else if (usuario =="inativo")
                                {
                                    MessageBox.Show("você não tem permissão para acessar!!!");
                                    txtEmail.Clear();
                                    Pass.Clear();
                                    txtEmail.Focus();
                                    PbgLogin.Value = 0;
                                    Pass.IsEnabled = false;
                                    btnEntrar.IsEnabled = false;
                                    txbCarrega.Text = "0%";
                                }
                                break;
                            }

                        });

                    }

                });
            }
            catch
            {
                MessageBox.Show("Favor preencher o usuario ou a senha corretamente!!!");
                txtEmail.Clear();
                Pass.Clear();
                txtEmail.Focus();
                PbgLogin.Value = 0;
                Pass.IsEnabled = false;
                btnEntrar.IsEnabled = false;
                txbCarrega.Text = "0%";
            }
        }
    }
}
